.. SPDX-License-Identifier: CC-BY-SA-4.0

*******************
About This Document
*******************

Introduction
============

Firmware updates in the Arm ecosystem have been historically handled by proprietary methods.
Some level of firmware behavior and lifecycle standardization is required to open business
opportunities in a broader ecosystem.

Part of the firmware lifecycle is the update process.  Firmware and firmware update technologies
are very platform dependent but the process can be standardized assuming that firmware will
cover all the platform specific aspects.

**The firmware update is driven by the main operating system through the standard UEFI update**
**capsule technology, to ensure the update process is independent from the processor and OS**
**implementation.**
**The creation of update capsules and their signature(s) are outside the scope of the document.**

This document addresses the robustness of the update process in order to provide:

- Brick protection (not limited to firmware upgrades)
- Rollback capabilities (if permitted)
- Rollback protection

While it is assumed that, in real life scenarios, there will be a lot of testing prior to deploying
an update, unexpected behavior can still happen due to hardware revision differences, language
configuration, operational conditions (power, temperature...) amongst other reasons.
For the reasons above committing the updates and allowing the new firmware to be used must be
verified and approved by the OS.  Each vendor can have his application verifying the firmware
operation before accepting it.  This is going to be referred as 'transactional updates' for the
rest of the document.

The intent of this document is to provide guidelines for firmware updates that will protect the
device against bricking and rollback attacks. The update itself may occur from the non-secure world
(Non-secure firmware) or the secure world (Secure firmware) depending on the implementation and the hardware requirements.

*Comments or change requests can be sent to `boot-architecture@lists.linaro.org`.*

Scope
=====
Dependable Boot was written as a response to the lack of firmware upgrade standardization in the
embedded system ecosystem.
As embedded systems are becoming more sophisticated and connected, it is becoming increasingly
important for embedded systems to standardize the way they upgrade their firmware.
The document aims at protecting devices against unauthorized updates, defective
updates and hardware failures.

Requirements and Key concepts
=============================

Prerequisites
-------------
Since this document targets [EBBR]_ and [PSBG]_ compliant systems, the update process of platforms
without UEFI is outside the scope of this document.

- Every firmware binary **must** support dual A/B partitions for all of it's
  components.
- The first stage bootloader **must** be able to access our firmware NVCounter.
- UEFI capsule updates are the mechanism used for delivering firmware updates. 
  Refer to [#UEFICapsuleUpdateNote]_ for details.
- We assume that our immutable stage loader can load the second stage boot loader (e.g TF-A) from a dual image.
  In case the masked ROM can not boot with an alternative location, similar functionality
  **must** be added to the secondary bootloader stage. In that case the secondary boot loader inherits all the
  requirements of the first stage boot loader and the secondary bootloader stage is disallowed from OTA updates. 
  In person update of secondary bootloader stage may be possible and may require extra
  tools or procedures but that is out of the scope of this document.
- Update process can target a single firmware component or multiple components.  Updating multiple
  components and multiplexing boot combinations can be very challenging.  In this document we treat
  the firmware as a single entity regardless of the components it comprises.
  Failing to update one of the components will result in an overall failure,
  forcing the firmware to remain on the current bank.
- Updating the firmware and the OS at the same time is prohibited.
- A hardware watchdog **must** always be active when critical components are executed.
  It's advisable the watchdog is activated on the earliest possible boot stages.
  The watchdog **must** reset the board if a timeout occurs.

.. [#UEFICapsuleUpdateNote] [UEFI]_ 2.8B § 23 - Firmware Update and Reporting

Transactional updates
---------------------
Since firmware updates are independent from the OS upgrades, an important aspect of the update
policy is the device functionality.  If the OS ends up being unusable after a firmware update (while the platform is in the Trial state [FWU]_),
the device must be able to detect this and force the platform to the previously working firmware.
Two scenarios can be considered:

#. The FW acceptance tests, executed by the OS, fail: The OS must request a FW downgrade by using the methodology described in :ref:`revert-sec`.

#. The OS fails to boot, e.g. due to a watchdog timer fire: The Normal World FW must keep a counter of the attempted OS boots in the Trial State. If the platform is in the Trial State after a platform specific number of OS boot attempts, the Normal World FW must take the steps to revert the FW to the previous active FW bank.

If the FW acceptance tests, executed by the OS, pass, then the newly installed FW can be made permanent.
To mark the FW as permanent the OS  will install an acceptance capsule as described in :ref:`OS directed FW image acceptance`.
The Non-secure firmware will, on next reboot, mark all FW images in the active bank as accepted and
update the rollback counters to the new values (if applicable) [FWU]_.
Once all the FW images in the active bank are accepted, the device transitions from the Trial state to the Regular state [FWU]_.

.. image:: uml/update_overview.eps
  :width: 400px
  :align: center

Rollback protection
-------------------
Nodes need to provide protection against rollback based attacks.
If at any point the device firmware was updated to patch security vulnerabilities, allowing
rollback to any previous insecure versions is a security risk.  It's possible for a non-persistent
attack to download, flash an older vulnerable version of the firmware and install a permanent
exploit on the device.  Downgrading to a previous version should be prohibited but in case an older
version is detected the device can check the backup partitions.  If a valid version is found on
the secondary partition the device can reboot using that.  If both partitions are invalid the device
will go into a reboot loop.

Brick protection
----------------
The brick protection mechanism not only addresses the bricking during the firmware update, but
can optionally protect the device against hardware failures.  The rollback counter might or 
might not be always bumped during an update.  If the secondary partition contains a valid 
firmware and the primary partition is unable to boot the device (e.g flash corruption),  
the device is allowed to fallback on the secondary partition.

If the update is going to bump the rollback counters it's strongly advised to update both of the
partitions.  In that case the upgrade process will run once to update the secondary partition.  Once
that's finished and accepted,  the firmware update agent should update the former primary partition
as well.  This process must not necessarily go through the entire update procedure.  Simply writing
and verifying the firmware is enough.
Similarly if an update of the secondary partition fails (e.g write failed),
then the firmware update agent must restore the partition to a known working
state (e.g overwrite it with the current working firmware)

.. image:: uml/rollback_protection_simple.eps
  :width: 200px
  :align: center
