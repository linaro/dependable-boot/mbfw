@startuml
[*] -> StableA
StableA #6b8724 ---> StagingB #CE5756: CapsuleUpdate()
StagingB -> StableA: Aborted or Failed
StagingB -> TrialB #0093a6 : Trial Reboot
TrialB -> StableB #7773cf : Success
TrialB -> StableA : Failure
StableB ---> StableA : Rollback (if supported)

@enduml
