@startuml
skinparam sequenceMessageAlign center
participant "TF-A" as TFA #CE5756
participant "OP-TEE" as OPTEE #6b8724
participant Firmware #0093a6
participant "OS" as Linux #7773cf

Linux -> Firmware : CapsuleUpdate()\n  Schedule CapsuleUpdate on reboot
Linux -> TFA: Reboot (optional)
note over TFA,Firmware #cccc00: Update secondary partition
Firmware -> OPTEE: Signal CapsuleUpdate - Update Pending
Firmware -> TFA: Signal CapsuleUpdate - Update Pending
Firmware -> TFA: Reboot
note left of TFA #fdb702:  Trial run.\n Boot from new partition.
TFA --> Linux: Update pending
note right of Linux #149414:  OS tests success.
Linux -> Firmware : CapsuleUpdate()\n Create a empty Capsule to signal 'update done'
Firmware -> OPTEE: Propagate CapsuleUpdate - Done
Firmware -> TFA: Propagate CapsuleUpdate - Done
...some time later...
Linux -> TFA: Reboot.
note left of TFA #fdb702:  Confirm transaction.\n Update rollback counters.\n Boot from new partition.
Firmware -> Linux: Set ESRT tables for successful upgrade
note right of Linux #cccc00: Push notifications upgrade
@enduml
