
***********
Conventions
***********

Conventions Used in this Document
=================================

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be
interpreted as described in :rfc:`2119`.

Terms and abbreviations
=======================

This document uses the following terms and abbreviations.

.. glossary::

   UEFI
      Unified Extensible Firmware Interface.

   Anti-brickable
      A system is said to be a brick if it cannot boot for a firmware behavior issue
      and no firmware update is possible.  Anti-Brickable protection is a set of measures
      to protect against this risk for any firmware component.

   Boot firmware
      Firmware that brings up operating system

   EBBR
      Embedded Base Boot Requirements

   ROM
      Read only memory

   Immutable bootloader stage (Immutable stage)
      First bootloader stage, generally stored in a ROM -- equatable to BL1 in TF-A or u-boot-spl.

   Secondary bootloader stage (Secondary stage)
      Optional bootloader stage that executes following the immutable stage -- equatable to BL2 in TF-A.

   Monitor Firmware
      EL3 Runtime Firmware -- refered to as BL31 in TF-A.

   Secure firmware
      Firmware executing at S-EL2 and/or S-EL1 -- BL32 in the TF-A context. The Secure payload can be composed of different binaries.

   Non-secure firmware
      Firmware executing at EL2 or EL1 -- BL33 in the TF-A context.

   SCP Firmware
      System Control Processor firmware

   PSGB
      Platform Security Boot Guide

   Trusted Substrate
      Set of firmwares that control security and trust aspects of a platform.
      For instance device identity management firmware.

   FFA
      Arm Firmware Framework for Armv-8A


