# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = sphinx-build
SPHINXPROJ    = DB
SOURCEDIR     = source
BUILDDIR      = build

IMG_SRC_DIR = source/uml
IMG_TGTS = $(patsubst %.txt,%.eps,$(wildcard $(IMG_SRC_DIR)/*.txt))

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help Makefile

%.txt:;

%.eps: %.txt
	plantuml -teps $?

images: ${IMG_TGTS};

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile images
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
